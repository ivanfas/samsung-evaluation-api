package com.example.demo.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.example.demo.dto.DomainDto;

@Component
public final class DemoUtils {
	public static <E extends Enum<E>> List<DomainDto> listDomain(Class<E> enumData, 
			MessageSource messageSource) {

		String labelPattern = enumData.getCanonicalName() + ".";
		List<DomainDto> elements = new ArrayList<>();
		for (Enum<E> enumVal : enumData.getEnumConstants()) {
			elements.add(new DomainDto(enumVal.toString(),
					messageSource.getMessage(labelPattern + enumVal.toString(), 
							null, null)));
		}
		return elements;
	}
}
