package com.example.demo.controller;
import static com.example.demo.utils.DemoUtils.listDomain;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.DocDomain;
import com.example.demo.dto.DomainDto;

@RestController
@RequestMapping("/api/domain")
class DomainController {
	
	@Autowired
	private MessageSource messageSource;

    @GetMapping("/doc/moeda")
    Collection<DomainDto> moedas() {
        return listDomain(DocDomain.Moeda.class, messageSource);
    }
 
}