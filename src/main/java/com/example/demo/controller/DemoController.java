package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DemoDocDto;
import com.example.demo.dto.DocFilter;
import com.example.demo.service.DemoService;

@RestController
@RequestMapping("/api")
public class DemoController {
	
	@Autowired
	DemoService service;
	
	@GetMapping("/docs")
	List<DemoDocDto> getDocs(DocFilter docFilter
			) {
		
		return service.getDemoDocDtos(docFilter.getDocumentNumber(), 
				null, null, null);
	}

}
