package com.example.demo.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class DemoDocDto {
	private String documentNumber;
	private LocalDate documentDate;
	private String currencyCode;
	private BigDecimal documentValue;

	// from Currency
	private String currencyDesc;

	// from Quotation
	private BigDecimal usdValue;
	private BigDecimal penValue;
	private BigDecimal brlValue;

	public DemoDocDto(DocDto docDto) {
		this.documentNumber = docDto.getDocumentNumber();
		this.documentDate = docDto.getDocumentDate();
		this.currencyCode = docDto.getCurrencyCode();
		this.documentValue = docDto.getDocumentValue();
	}

	public DemoDocDto() {
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public LocalDate getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(LocalDate documentDate) {
		this.documentDate = documentDate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public BigDecimal getDocumentValue() {
		return documentValue;
	}

	public void setDocumentValue(BigDecimal documentValue) {
		this.documentValue = documentValue;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	public BigDecimal getUsdValue() {
		return usdValue;
	}

	public void setUsdValue(BigDecimal usdValue) {
		this.usdValue = usdValue;
	}

	public BigDecimal getPenValue() {
		return penValue;
	}

	public void setPenValue(BigDecimal penValue) {
		this.penValue = penValue;
	}

	public BigDecimal getBrlValue() {
		return brlValue;
	}

	public void setBrlValue(BigDecimal brlValue) {
		this.brlValue = brlValue;
	}

}
