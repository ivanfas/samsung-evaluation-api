package com.example.demo.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuotationDto {

	private String fromCurrencyCode;
	private String toCurrencyCode;
	private BigDecimal cotacao;
	private LocalDate dataHoraCotacao;

	public String getFromCurrencyCode() {
		return fromCurrencyCode;
	}

	public void setFromCurrencyCode(String fromCurrencyCode) {
		this.fromCurrencyCode = fromCurrencyCode;
	}

	public String getToCurrencyCode() {
		return toCurrencyCode;
	}

	public void setToCurrencyCode(String toCurrencyCode) {
		this.toCurrencyCode = toCurrencyCode;
	}

	public BigDecimal getCotacao() {
		return cotacao;
	}

	public void setCotacao(BigDecimal cotacao) {
		this.cotacao = cotacao;
	}

	public LocalDate getDataHoraCotacao() {
		return dataHoraCotacao;
	}

	public void setDataHoraCotacao(LocalDate dataHoraCotacao) {
		this.dataHoraCotacao = dataHoraCotacao;
	}
	
	public Long getDataHoraCotacaoLong() {
		return dataHoraCotacao.toEpochDay();
	}
}
