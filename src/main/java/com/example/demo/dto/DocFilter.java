package com.example.demo.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocFilter {
	String documentNumber;
	String currencyCode;
	LocalDate documentDateStart;
	LocalDate documentDateEnd;
}
