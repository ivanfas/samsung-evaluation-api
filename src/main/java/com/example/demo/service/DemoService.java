package com.example.demo.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.CurrencyDto;
import com.example.demo.dto.DemoDocDto;
import com.example.demo.dto.DocDto;
import com.example.demo.dto.QuotationDto;

@Service
public class DemoService {
	@Autowired
	RestTemplate restTemplate;

	@Value("https://cellolatam.cellologistics.com.br/sds-devs-evaluation/evaluation/currency")
	String currencyURL;

	@Value("https://cellolatam.cellologistics.com.br/sds-devs-evaluation/evaluation/quotation")
	String quotationURL;

	@Value("https://cellolatam.cellologistics.com.br/sds-devs-evaluation/evaluation/docs")
	String docsURL;
	
	public List<DemoDocDto> getDemoDocDtos(String documentNumber, String currencyCode, 
			LocalDate documentDateStart, LocalDate documentDateEnd) {
		
		List<DemoDocDto> result = new ArrayList<>();
		DocDto[] docs = getDocs();
		CurrencyDto[] currencies = getCurrencies();
		QuotationDto[] quotations = getQuotations();
		
		List<DocDto> filteredDocs = filterDocs(documentNumber, currencyCode, documentDateStart, documentDateEnd, docs);
		
		for(DocDto docDto : filteredDocs) {
			DemoDocDto demoDocDto = new DemoDocDto(docDto);
			demoDocDto.setCurrencyDesc(resolveCurrencyDesc(docDto.getCurrencyCode(), currencies));
			demoDocDto.setUsdValue(resolveQuotation(docDto, quotations, "USD"));
			demoDocDto.setPenValue(resolveQuotation(docDto, quotations, "PEN"));
			demoDocDto.setBrlValue(resolveQuotation(docDto, quotations, "BRL"));
			result.add(demoDocDto);
		}
		return result;
	}
	
	private CurrencyDto[] getCurrencies() {
		ResponseEntity<CurrencyDto[]> response = restTemplate.getForEntity(currencyURL, CurrencyDto[].class);
		CurrencyDto[] currencies = response.getBody();
		return currencies;
	}
	
	private QuotationDto[] getQuotations() {
		ResponseEntity<QuotationDto[]> response = restTemplate.getForEntity(quotationURL, QuotationDto[].class);
		QuotationDto[] quotations = response.getBody();
		return quotations;
	}
	
	private DocDto[] getDocs() {
		ResponseEntity<DocDto[]> response = restTemplate.getForEntity(docsURL, DocDto[].class);
		DocDto[] docs = response.getBody();
		return docs;
	}
	
	private List<DocDto> filterDocs(String documentNumber, String currencyCode, LocalDate documentDateStart,
			LocalDate documentDateEnd, DocDto[] docs) {
		List<DocDto> filteredDocs = Arrays.asList(docs).stream()
				.filter(docDto -> documentNumber == null || docDto.getDocumentNumber().equals(documentNumber))
				.filter(docDto -> currencyCode == null || docDto.getCurrencyCode().equals(currencyCode))
				.filter(docDto -> documentDateStart == null || docDto.getDocumentDate().isAfter(documentDateStart))
				.filter(docDto -> currencyCode == null || docDto.getDocumentDate().isBefore((documentDateEnd)))
				.collect(Collectors.toList());
		return filteredDocs;
	}

	private BigDecimal resolveQuotation(DocDto docDto, QuotationDto[] quotations, String currencyTo) {
		List<QuotationDto> quotationDtos =  Arrays.asList(quotations);
		if(currencyTo.equals(docDto.getCurrencyCode())) {
			return docDto.getDocumentValue();
		}
		
		List<QuotationDto> filteredQuotations = quotationDtos.stream()
				.filter(quotationDto -> docDto.getCurrencyCode().equals(quotationDto.getFromCurrencyCode()))
				.filter(quotationDto -> currencyTo.equals(quotationDto.getToCurrencyCode()))
				.sorted(Comparator.comparingLong(QuotationDto::getDataHoraCotacaoLong).reversed())
				.collect(Collectors.toList());
		
		QuotationDto lastQuotationDto = filteredQuotations.stream()
				.findFirst()
				.orElse(new QuotationDto());
		
		return quotationDtos.stream()
				.filter(quotationDto -> docDto.getDocumentDate().equals(quotationDto.getDataHoraCotacao()))
				.findAny()
				.orElse(lastQuotationDto)
				.getCotacao();
	}

	private String resolveCurrencyDesc(String currencyCode, CurrencyDto[] currencies) {
		List<CurrencyDto> currencyDtos =  Arrays.asList(currencies); 
		return currencyDtos.stream()
				.filter(currencyDto -> currencyCode.equals(currencyDto.getCurrencyCode()))
				.findAny()
				.orElse(new CurrencyDto())
				.getCurrencyDesc();
	}
}
